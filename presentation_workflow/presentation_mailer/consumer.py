import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    message = json.loads(body)
    print("Received %r"%body)
    presenter_name = message["presenter_name"]
    title = message["title"]
    presenter_email = message["presenter_email"]
    message_body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        'Your presentation has been accepted',
        message_body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    print("Rejected %r"%body)
    presenter_name = message["presenter_name"]
    title = message["title"]
    presenter_email = message["presenter_email"]
    message_body = f"{presenter_name}, we're sad to tell you that your presentation {title} has been rejected"
    send_mail(
        'Your presentation has been rejected',
        message_body,
        'admin@conference.go',
        [presenter_email],
        fail_silently=False,
    )


def main():
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to rabbitmq")
        time.sleep(2.0)


if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print("Interupted")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
